require "set"

class Integer
    def money
        sign = self < 0 ? "-" : ""
        "#{sign}#{self.abs / 100},#{self.abs % 100}"
    end
end

class Entry
    attr_accessor :book_date
    attr_accessor :transaction_date
    attr_accessor :description
    attr_accessor :amount
    attr_accessor :balance

    attr_accessor :category

    def ==(other)
        book_date == other.book_date &&
        transaction_date == other.transaction_date &&
        description == other.description &&
        amount == other.amount &&
        balance == other.balance
    end

    def hash
        book_date.hash * 1 +
        transaction_date.hash * 2 +
        description.hash * 5 +
        amount.hash * 7 +
        balance.hash * 8
    end

    alias :eql? :==
end

class EntrySet < Set
    def group_by(&block)
        super(&block).map { |key, entries| [key, EntrySet.new(entries)] }
    end

    def expenditures
        select { |entry| entry.amount < 0 }
    end

    def total_expenditure
        expenditures.map(&:amount).reduce(:+) || 0
    end

    def incomes
        select { |entry| entry.amount > 0 }
    end

    def total_income
        incomes.map(&:amount).reduce(:+) || 0
    end

    def sum
        map(&:amount).reduce(:+)
    end
end