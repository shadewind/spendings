require "browser"
require "time"
require "money"

class Swedbank < Browser
    INIT_PATH = "/bviPrivat/privat?ns=1"
    USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"

    def initialize
        super("internetbank.swedbank.se", use_ssl: true)
        self.user_agent = USER_AGENT
    end

    def login(pnr)
        init_redirect_page = post_form_page(path: INIT_PATH)
        method_page = post_form_page(page: init_redirect_page, form_data: { "form1:fortsett_knapp" => "klicka" })


        form_data = {
            "auth:kundnummer" => pnr,
            "auth:metod_2" => "ACTCARD",
            "auth:fortsett_knapp" => "Fortsätt"
        }

        auth_page = post_form_page(page: method_page, form_data: form_data)
        auth_html = Nokogiri::HTML(auth_page.body)
        challenge = auth_html.xpath("//span[span/@class='authBorder']").text[/\d{4} \d{4}/]

        form_data = {
            "form:challengeResponse" => yield(challenge),
            "form:fortsett_knapp" => "Fortsätt"
        }

        login_redirect_page = post_form_page(page: auth_page, form_data: form_data, form_name: "form")
        logged_in_page = post_form_page(page: login_redirect_page)
        puts logged_in_page.body
    end

    def self.scrape_page(page)
        html = Nokogiri::HTML(page, nil, "ISO_8859_1")

        rows = html.xpath(
            "//h3[contains(text(), 'senaste transaktioner')]/" +
            "following::tr[contains(td/@class, 'tabell-cell')]")

        rows.map do |row|
            cells = row.xpath("td").map do |cell|
                cell.text.gsub("\u00A0", "").strip
            end

            entry = Entry.new
            entry.book_date = Time.parse(cells[0])
            entry.transaction_date = Time.parse(cells[1])
            entry.description = cells[2]
            entry.amount = self.parse_money(cells[4])
            entry.balance = self.parse_money(cells[5])
            entry
        end
    end

    def self.parse_money(str)
        if str.gsub("\s", "") =~ /(-?)(\d+)(?:,(\d\d))/
            sign = ($1 == "-") ? -1 : 1
            amount = $2.to_i
            cents = $3.to_i || 0

            sign * ((amount * 100) + cents)
        else
            nil
        end
    end
end