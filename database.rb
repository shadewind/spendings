require "psych"
require "money"

module Database

    class << self

        def add_entry(entry)
            db.add?(entry)
        end

        def save
            File.open("data.yml", "w") { |io| Psych.dump(db.to_a, io) }
        end

        def db
            if @db.nil?
                begin
                    @db = EntrySet.new(Psych.load_file("data.yml"))
                rescue Errno::ENOENT
                    @db = EntrySet.new
                end
            end

            @db
        end

    end
end